import matplotlib.pyplot as plt
import numpy as np

# Generate random student scores (example data)
np.random.seed(42)
student_scores = np.random.normal(loc=70, scale=15, size=100)

# Create a histogram plot
plt.hist(student_scores, bins=20, color='skyblue', edgecolor='black')

# Add labels and title
plt.xlabel('Student Scores')
plt.ylabel('Frequency')
plt.title('Distribution of Student Scores')

# Display the plot
plt.show()

