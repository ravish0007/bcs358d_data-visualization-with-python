import matplotlib.pyplot as plt

# Hypothetical data: Number of FIFA World Cup wins for different countries
countries = ['Brazil', 'Germany', 'Italy', 'Argentina', 'Uruguay', 'France', 'England', 'Spain']
wins = [5, 4, 4, 3, 2, 2, 1, 1]  # Replace with actual data

# Colors for each country
colors = ['yellow', 'magenta', 'green', 'blue', 'lightblue', 'blue', 'red', 'cyan']

# Create a pie chart
plt.pie(wins, labels=countries, autopct='%1.1f%%', colors=colors, startangle=90)

# Add title
plt.title('FIFA World Cup Wins by Country')

# Display the plot
plt.axis('equal')  # Equal aspect ratio ensures that the pie chart is circular.
plt.show()



