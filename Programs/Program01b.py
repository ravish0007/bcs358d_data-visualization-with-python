#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 21 16:35:54 2023

@author: putta
"""

from collections import Counter

value = str(input("Enter a value : "))
if value == value[::-1]:
    print("Palindrome")
else:
    print("Not Palindrome")
    
counted_dict = Counter(value)
for key in sorted(counted_dict.keys()):
    print(f'{key} appears {counted_dict[key]} times');

